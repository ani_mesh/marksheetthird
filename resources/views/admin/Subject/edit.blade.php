@extends('layouts.new')
@section('content')
<div class="banner">
  <h2>
  <a href="#">Home</a>
  <i class="fa fa-angle-right"></i>
  <span>Edit subject</span>
  </h2>
</div>
<!--//banner-->

<div class="content-top">
  
  
  
  <div class="grid-form1">
    <form class="form-horizontal" action="{{route('subject.update',['subject_id'=>$subject->id])}}" method="post">
      {{ csrf_field() }}
     <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label hor-form">Subject Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputEmail3" placeholder="" name="name" value="{{$subject->name}}">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label hor-form">Credit Hour</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputPassword3" placeholder="" name="credit" value="{{$subject->credit_hours}}">
    </div>
  </div>
      <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Save</button>
    </div>
  </div>
      
    </form>
  </div>
  
</div>
</div>
@stop
@section('style')
<style type="text/css">
  table, th, td {
border: 1px solid black;
}
</style>
@stop
@section('scripts')
<script type="text/javascript">
var tbody = $("#tbody");
if (tbody.children().length == 0) {
$('#save').hide();
tbody.append('<tr><th colspan="5" class="text-center">All mark of Subject is added</th></tr>');
}
</script>
@stop